import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import libraryApp from '../reducers/index';

export default function configureStore() {
    return createStore(
        libraryApp,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        applyMiddleware(thunk)
        
    );
}