import axios from 'axios';
import * as actionTypes from '../constants/Books';
import { api } from '../etc/Api';

export const getBooks = () => {
    return(dispatch) => {
         axios.get(`${api.apiPrefix}/getbooks`)
        .then(response => {
            dispatch({
                type: actionTypes.GET_BOOKS, 
                books: response.data
            });
        })
        .catch(err => {
            throw(err)
        }) 
    }
};

export const getKSTUBooks = () => {
    return(dispatch) => {
        return axios.get(`${api.apiPrefix}/getkstuworks`)
        .then(response => {
            dispatch({
                type: actionTypes.GET_KSTU_BOOKS, 
                kstuBooks: response.data
            });
        })
        .catch(err => {
            throw(err)
        }) 
    }
};

export const getNTBBooks = () => {
    return(dispatch) => {
        return axios.get(`${api.apiPrefix}/getntbworks`)
        .then(response => {
            dispatch({
                type: actionTypes.GET_NTB_BOOKS, 
                ntbBooks: response.data
            });
        })
        .catch(err => {
            throw(err)
        }) 
    }
};

export const downloadBookById = (bookId) => {
    return (dispatch) => {
        return axios.get(`${api.apiPrefix}/book/${bookId}`)
            .then(response => {
                dispatch({
                    type: actionTypes.DOWNLOAD_BOOK_BY_ID,
                    book: response.data
                });
            })
            .catch(err => {
                throw(err)
            })
    }
};

export const uploadBookRequest = (data) => {
    return (dispatch) => {
        return axios.post(`${api.apiPrefix}/files`, data)
            .then(response => {
                dispatch({
                    type: actionTypes.UPLOAD_BOOK_SUCCESS,
                    book: response
                });
            })
            .catch(err => {
                dispatch({
                    type: actionTypes.UPLOAD_BOOK_FAIL,
                    error: err
                })
            })
    }
};

export const addBook = (data) => {
    console.log(data, 'actionData');
    return (dispatch) => {
        return axios.post(`${api.apiPrefix}/add-book`, data)
            .then(response => {
                dispatch({
                    type: actionTypes.ADD_BOOK_SUCCESS,
                    book: response.data
                });
            })
            .catch(err => {
                dispatch({
                    type: actionTypes.ADD_BOOK_FAIL,
                    book: err
                })
            })
    }
};