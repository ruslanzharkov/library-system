import axios from 'axios';
import * as actionTypes from '../constants/Objects';
import { api } from '../etc/Api';

export const getObjects = () => {
    return(dispatch) => {
        axios.get(`${api.apiPrefix}/objects`)
            .then(response => {
                dispatch({
                    type: actionTypes.GET_OBJECTS,
                    languages: response.data
                });
            })
            .catch(err => {
                throw(err)
            })
    }
};