import axios from 'axios';
import * as actionTypes from '../constants/Languages';
import { api } from '../etc/Api';

export const getLanguages = () => {
    return(dispatch) => {
        axios.get(`${api.apiPrefix}/languages`)
            .then(response => {
                dispatch({
                    type: actionTypes.GET_LANGUAGES,
                    languages: response.data
                });
            })
            .catch(err => {
                throw(err)
            })
    }
};

export const addLanguage = (data) => {
    return(dispatch) => {
        axios.post(`${api.apiPrefix}/language-add`, data)
            .then(response => {
                dispatch({
                    type: actionTypes.ADD_LANGUAGES_SUCCESS,
                    language: response.data
                });
            })
            .catch(err => {
                dispatch({
                    type: actionTypes.ADD_LANGUAGES_FAIL,
                    language: err
                });
            })
    }
};