import axios from 'axios';
import * as actionTypes from '../constants/Lyceum';
import { api } from '../etc/Api';

export const getLyceum = () => {
    return(dispatch) => {
        axios.get(`${api.apiPrefix}/lyceum`)
            .then(response => {
                dispatch({
                    type: actionTypes.GET_LYCEUM,
                    lyceums: response.data
                });
            })
            .catch(err => {
                throw(err)
            })
    }
};

export const addLyceum = (data) => {
    return(dispatch) => {
        axios.post(`${api.apiPrefix}/lyceum-add`, data)
            .then(response => {
                dispatch({
                    type: actionTypes.ADD_LYCEUM_SUCCESS,
                });
            })
            .catch(err => {
                dispatch({
                    type: actionTypes.ADD_LYCEUM_FAIL,
                });
            })
    }
};