import axios from 'axios';
import * as actionTypes from '../constants/Statistics';
import { api } from '../etc/Api';

export const getVisits = () => {
    return(dispatch) => {
        axios.get(`${api.apiPrefix}/visits`)
            .then(response => {
                dispatch({
                    type: actionTypes.GET_STATISTICS,
                    statistics: response.data
                });
            })
            .catch(err => {
                throw(err)
            })
    }
};