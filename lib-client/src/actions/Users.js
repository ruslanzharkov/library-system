import axios from 'axios';
import * as actionTypes from '../constants/Users';
import { api } from '../etc/Api';

export const authUser = (authData) => {
    return(dispatch) => {
        return axios.post(`${api.apiPrefix}/authenticate`, authData)
        .then(response => {

            let token = response.data.token;
            let user = response.data.user;

            if (token && user) {
                localStorage.setItem('token', token);
                localStorage.setItem('userId', user);

                dispatch(authSuccess({user: user, token: token}));
            } else {
                localStorage.removeItem('token');
            }
            dispatch({
                type: actionTypes.AUTH,
                users: user
            })
        })
        .catch(err => {
            throw(err);
        })
    }
};


export const authSuccess = (authData) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        authData: authData
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};


