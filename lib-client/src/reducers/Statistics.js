import * as actions from '../constants/Statistics';

export const statistics = (state = [], action) => {
    switch(action.type) {
        case actions.GET_STATISTICS:
            return action.statistics;
        default:
            return state;
    }
};

