import * as actions from '../constants/Books';

export const books = (state = [], action) => {
	switch(action.type) {
        case actions.GET_BOOKS:
            return action.books;
        case actions.GET_KSTU_BOOKS:
            return action.kstuBooks;
        case actions.GET_NTB_BOOKS:
            return action.ntbBooks;
        case actions.DOWNLOAD_BOOK_BY_ID:
            return action.book;
        case actions.ADD_BOOK_SUCCESS:
            return action.book;
        case actions.ADD_BOOK_FAIL:
            return action.book;
    default:
        return state;
    } 
};

