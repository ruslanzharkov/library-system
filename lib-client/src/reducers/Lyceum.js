import * as actions from '../constants/Lyceum';

export const lyceums = (state = [], action) => {
    switch(action.type) {
        case actions.GET_LYCEUM:
            return action.lyceums;
        case actions.ADD_LYCEUM_SUCCESS:
            return {
                message: 'Добавление успешно!'
            };
        case actions.ADD_LYCEUM_FAIL:
            return {
                message: 'Произошла непредвиденная ошибка!'
            };
        default:
            return state;
    }
};


