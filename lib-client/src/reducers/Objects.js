import * as actions from '../constants/Objects';

export const objects = (state = [], action) => {
    switch(action.type) {
        case actions.GET_OBJECTS:
            return action.languages;
        default:
            return state;
    }
};

