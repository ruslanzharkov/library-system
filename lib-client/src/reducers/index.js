import { combineReducers } from 'redux';
import { books } from './Books';
import { users } from './Users';
import { objects } from './Objects';
import { languages } from "./Languages";
import { lyceums } from "./Lyceum";
import { statistics } from './Statistics';

const reducers = combineReducers({
    books: books,
    users: users,
    objects: objects,
    languages: languages,
    lyceums: lyceums,
    statistics: statistics
});

export default reducers;