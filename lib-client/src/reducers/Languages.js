import * as actions from '../constants/Languages';

export const languages = (state = [], action) => {
    switch(action.type) {
        case actions.GET_LANGUAGES:
            return action.languages;
        case actions.ADD_LANGUAGES_SUCCESS:
            return action.language;
        case actions.ADD_LANGUAGES_FAIL:
            return action.language;
        default:
            return state;
    }
};

