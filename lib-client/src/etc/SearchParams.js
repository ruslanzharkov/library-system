export const searchParams =  [
    {name: "autor_books", value: "По автору"},
    {name: "name_books", value: "По названию"},
    {name: "about_books", value: "По ключевому слову"},
    {name: "object_books", value: "По рубрике"},
    {name: "year_books", value: "По году издания"},
    {name: "lang_books", value: "По языку"}
];