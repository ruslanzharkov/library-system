import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';

const style = {
    paper: {
        display: 'inline-block',
        float: 'left',
        textAlign: 'left',
        marginLeft: '30px',
        marginTop: '40px'
    },
        rightIcon: {
        textAlign: 'center',
        lineHeight: '24px',
    },
};

class MenuExampleIcons extends Component {
    render() {
        return (
            <div>
                <Paper style={style.paper}>
                    <Menu>
                        <MenuItem
                            primaryText="Посмотреть книги"
                            leftIcon={<i className="material-icons">chrome_reader_mode</i>}
                            onClick={this.props.showBooks}
                        />
                        <MenuItem
                            primaryText="Добавить книгу"
                            leftIcon={<i className="material-icons">picture_as_pdf</i>}
                            onClick={this.props.bookAdd}
                        />
                        <Divider />
                        <MenuItem
                            primaryText="Посмотреть рубрику"
                            leftIcon={<i className="material-icons">list</i>}
                            onClick={this.props.showObjects}
                        />
                        <MenuItem
                            primaryText="Добавить рубрику"
                            leftIcon={<i className="material-icons">add</i>}
                            onClick={this.props.subjectAdd}
                        />
                        <Divider />
                        <MenuItem
                            primaryText="Посмотреть языки"
                            leftIcon={<i className="material-icons">list</i>}
                            onClick={this.props.showLanguages}
                        />
                        <MenuItem
                            primaryText="Добавить язык"
                            leftIcon={<i className="material-icons">add</i>}
                            onClick={this.props.languageAdd}
                        />
                        <Divider />
                        <MenuItem
                            primaryText="Посмотреть классы"
                            leftIcon={<i className="material-icons">list</i>}
                            onClick={this.props.showClasses}
                        />
                        <MenuItem
                            primaryText="Добавить класс"
                            leftIcon={<i className="material-icons">add</i>}
                            onClick={this.props.classAdd}
                        />
                        <Divider />
                        <MenuItem
                            primaryText="Посмотреть пользователей"
                            leftIcon={<i className="material-icons">person</i>}
                        />
                        <MenuItem
                            primaryText="Добавить пользователя"
                            leftIcon={<i className="material-icons">person_add</i>}
                        />
                        <Divider />
                        <MenuItem
                            primaryText="Статистика посещений"
                            leftIcon={<i className="material-icons">insert_chart</i>}
                            onClick={this.props.showUsage}
                        />
                        <MenuItem
                            primaryText="Статистика скачиваний"
                            leftIcon={<i className="material-icons">file_download</i>}
                        />
                    </Menu>
                </Paper>
            </div>
        );
    }
}

export default MenuExampleIcons;