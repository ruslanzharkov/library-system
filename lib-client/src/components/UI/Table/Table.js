import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import * as actions from '../../../actions/Books';
import {connect} from "react-redux";


class TableComponent extends Component {


    downloadFile = (book) => {
        this.props.downloadBook(book.id_books);

        setTimeout(() => {
            const response = {
                file: 'http://localhost:8080/' + this.props.book,
            };
            // server sent the url to the file!
            // now, let's download:

            window.open(response.file, '_blank');
        }, 100);
    };

    render() {
        return (
            <Table style={this.props.styles}>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    {this.props.labels.map(label => {
                        return <TableHeaderColumn key={label.name}>{label.name}</TableHeaderColumn>
                    })}
                </TableRow>
            </TableHeader>
            <TableBody
                displayRowCheckbox={false}
            >
                {this.props.data.map(item => {
                    return (
                        <TableRow key={item.id_books}>
                            <TableRowColumn>{item.autor_books}</TableRowColumn>
                            <TableRowColumn>{item.name_books}</TableRowColumn>
                            <TableRowColumn>{item.about_books}</TableRowColumn>
                            <TableRowColumn>{item.year_books}</TableRowColumn>
                            <TableRowColumn>{item.lang_books}</TableRowColumn>
                            <TableRowColumn>{item.size_books} мб</TableRowColumn>
                            <TableRowColumn>
                                <FlatButton
                                    label="Cкачать"
                                    onClick={() =>this.downloadFile(item)}
                                />
                            </TableRowColumn>
                        </TableRow>
                    )
                })}
            </TableBody>
            </Table>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        book: state.books
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        downloadBook: (id) => dispatch(actions.downloadBookById(id)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TableComponent);