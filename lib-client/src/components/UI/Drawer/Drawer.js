import React, {Component} from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Route , withRouter} from 'react-router-dom';

class DrawerUndocked extends Component {
    adminPageRedirect = () => {
        this.props.history.push({
            pathname: '/admin',
        })
    };

    mainPageRedirect = () => {
        this.props.history.push({
            pathname: '/search',
        })
    };

    logout =() => {
        this.props.history.push({
            pathname: '/',
        })
    };

    render() {
        return (
            <div>
                <Drawer
                    docked={false}
                    width={200}
                    open={this.props.open}
                    onRequestChange={this.props.requestChange}
                >
                    <MenuItem
                        leftIcon={<i className="material-icons">home</i>}
                        onClick={this.mainPageRedirect}>
                            Главная
                    </MenuItem>
                    <MenuItem
                        leftIcon={<i className="material-icons">dashboard</i>}
                        onClick={this.adminPageRedirect}>
                            Админпанель
                    </MenuItem>
                    <MenuItem
                        leftIcon={<i className="material-icons">account_box</i>}
                        onClick={this.handleClose}>
                            Учетная запись
                    </MenuItem>
                    <MenuItem
                        leftIcon={<i className="material-icons">reply</i>}
                        onClick={this.logout}>
                            Выйти
                    </MenuItem>
                </Drawer>
            </div>
        );
    }
}

export default withRouter(DrawerUndocked);