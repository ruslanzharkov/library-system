import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

class Input extends Component {
    render() {
        return(
            <TextField
                floatingLabelText={this.props.floatText}
                hintText={this.props.hintText}
                errorText={this.props.errorText}
                style={this.props.styles}
                type={this.props.type}
                onChange={this.props.onChange}
                multiLine={this.props.multiLine}
                rows={this.props.rows}
                rowsMax={this.props.rowsMax}
            />
        );
    }
}

export default Input;