import React, { Component } from 'react';
import RadioButton from 'material-ui/RadioButton';

class Radio extends Component {
    render() {
        return(
            <RadioButton
                checked={this.props.checked}
                label={this.props.label}
                onClick={this.props.onChange} 
            />
        )
    }
}

export default Radio;