import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

class Button extends Component {
    render() {
        return(
            <RaisedButton 
                onClick={this.props.onClick}
                label={this.props.label}
                style={this.props.styles}
                primary={this.props.primary}
                overlayStyle={this.props.backgroundColor}
                labelStyle={this.props.labelStyle}
            />
            
        )
    }
}

export default Button;