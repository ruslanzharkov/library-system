import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';

const styles = {
    title: {
      cursor: 'pointer',
    },
    background: '#2c98f0'
  };

class NavBar extends Component {
       render() {
       return(
           <AppBar
                style={styles}
                title="Электронно-библиотечная система КГТУ"
                iconClassNameRight="muidocs-icon-navigation-expand-more"
                onLeftIconButtonClick={this.props.burgerClicked}
            />
       )
   }
}

export default NavBar;