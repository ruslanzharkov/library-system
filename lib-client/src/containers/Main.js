import React, { Component } from 'react';
import Navbar from '../components/Navigation/Header/NavBar';
import './Main.css';
import SearchBooks from '../containers/SearchBooks/SearchBooks'
import Drawer from '../components/UI/Drawer/Drawer';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerOpen: false
        }
    }

    showDrawer = () => {
        this.setState({drawerOpen: !this.state.drawerOpen});
    };

    render() {
        return (
            <div>
                <div className='App'>
                    <Navbar
                        burgerClicked={this.showDrawer}
                    />
                </div>
                <div>
                    <SearchBooks store={this.props.books}/>
                </div>
                <Drawer
                    open={this.state.drawerOpen}
                    requestChange={(drawerOpen) => this.setState({drawerOpen})}
                />
            </div>
        );
    }
}

export default Main;
