import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '../../components/UI/Controls/Button';
import Input from '../../components/UI/Controls/Input';
import * as actions from '../../actions/Users';

import './LoginPage.css';

const inlineStyles = {
    input: {
        display: 'block',
    },
    button: {
        display: 'block',
        margin: 'auto',
        textAlign: 'center',
        marginTop: '15px',
        textColor: 'white',
        backgroundColor: '#2c98f0'
    }
};

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            errorTextLogin: '',
            errorLogin: false,
            errorTextPassword: '',
            errorPassword: false
        }
    }

    errorClear = () => {
        if (this.state.errorLogin) {
            this.setState({
                errorTextLogin: '',
                errorLogin: false
            })
        }

        if (this.state.errorPassword) {
            this.setState({
                errorTextPassword: '',
                errorPassword: false
            })
        }
    };

    loginChange = (event) => {
        this.setState({
            login: event.target.value,
            errorTextLogin: '',
            errorLogin: true
        });
        this.errorClear();
    };

    passwordChange = (event) => {
        this.setState({
            password: event.target.value,
            errorTextPassword: '',
            errorPassword: true
        });
        this.errorClear();
    };

    authUser = () => {
        if(!this.state.login) {
            this.setState({
                errorTextLogin: 'Поле логин не может быть пустым!'
            });
            return;
        }

        if(!this.state.password) {
            this.setState({
                errorTextPassword: 'Поле пароль не может быть пустым!'
            });
            return;
        }

        const user = {
            login: this.state.login,
            password: this.state.password
        };

        this.props.authUser(user)
            .then(this.authorization());

        console.log('hi')
    };

    authorization = () => {
        this.props.history.push({
            pathname: '/search',
        })
    };

    render() {
        return(
            <div className='ParentLogin'>
                 
                <div className='ChildLogin'>
                    <div className='BlockLogin'>
                        <h3 className='BlockLogin'>Добро пожаловать! Авторизуйтесь</h3>
                        <Input
                            floatText="Введите ваш логин"
                            styles={inlineStyles.input}
                            onChange={this.loginChange}
                            errorText={this.state.errorTextLogin}
                        />
                        <Input 
                            floatText="Введите ваш пароль"
                            styles={inlineStyles.input}
                            onChange={this.passwordChange}
                            type="password"
                            errorText={this.state.errorTextPassword}
                        />
                        <Button
                            label="Войти"
                            primary={true}
                            styles={inlineStyles.button}
                            backgroundColor={{backgroundColor: '#2c98f0'}}
                            onClick={this.authUser}
                        />
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        users: state.users
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        authUser: user => dispatch(actions.authUser(user))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);