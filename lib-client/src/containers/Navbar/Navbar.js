import React, { Component } from 'react';
import AppBar from '../../components/Navigation/Header/NavBar';

class Navbar extends Component {
    render() {
        return (
            <AppBar burgerClicked={this.props.burgerClicked}/>
        );
    }
}

export default Navbar;