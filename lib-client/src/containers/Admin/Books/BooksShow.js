import React, { PureComponent } from 'react';
import { Column, Table } from 'react-virtualized';
import 'react-virtualized/styles.css';
import * as actions from "../../../actions/Books";
import _ from 'lodash';
import {connect} from "react-redux";
import '../Books/BookUpload.css';
import Loader from '../../../components/Loader/Loader';

class BooksShow extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            books: []
        }
    }

    componentDidMount() {
        this.props.booksShow();
    }

    componentWillReceiveProps(nextProps) {
        if(this.props !== nextProps) {
            this.setState({
                books: nextProps.books
            })
        }
    }

    render() {
        let books = <Loader/>;
        if (!_.isEmpty(this.state.books)) {
            books = (
                <Table
                    width={1200}
                    height={600}
                    headerHeight={20}
                    rowHeight={30}
                    rowCount={this.state.books.length}
                    rowGetter={({ index }) => this.state.books[index]}
                    overscanRowCount={5}
                >
                    <Column
                        label='Автор книги'
                        dataKey='autor_books'
                        width={500}
                    />
                    <Column
                        width={700}
                        label='Название книги'
                        dataKey='name_books'
                    />

                </Table>
            )
        }

        return(
            <div>
                <div className="Header">Книги</div>
                <div className="Main">
                    {books}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        books: state.books
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        booksShow: () => dispatch(actions.getBooks())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BooksShow);