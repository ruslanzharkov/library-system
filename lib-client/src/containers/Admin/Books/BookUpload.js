import React, { Component } from 'react';
import Input from '../../../components/UI/Controls/Input';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import Button from '../../../components/UI/Controls/Button';
import './BookUpload.css';
import * as classActions from '../../../actions/Lyceum';
import * as objectActions from '../../../actions/Objects';
import * as languageActions from '../../../actions/Languages';
import * as bookActions from '../../../actions/Books';
import { connect} from "react-redux";

const inlineStyles ={
    input: {
        width: '350px',
    },
    dropdown: {
        width: '350px',
        verticalAlign: 'top',
        marginTop: '24px',
        textAlign:'left'
    },
    button: {
        verticalAlign: 'top',
        marginTop: '25px',
        marginBottom: '20px',
        width: '260px'
    },
    checkbox: {
        marginTop: 10,
    },
    block: {
        display: 'flex',
        margin: 'auto',
        marginLeft: '400px',
        marginRight: '200px'
    },
    block2: {
        margin: 10,
    },
};

class BookUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            objects: [],
            languages: [],
            classes: [],
            author: '',
            bookName: '',
            bookYear: '',
            bookDescription: '',
            bookKeyWords: '',
            bookPages: '',
            activeObject: null,
            activeLanguage: null,
            activeClass: null,
            file: null,
            kstuChecked: false,
            ntbChecked: false,
            bookSize: '',
            bookFileName: ''
        };
    }

    componentDidMount() {
        this.props.classesShow();
        this.props.objectsShow();
        this.props.languagesShow();
        this.setState({
            objects: this.props.objects,
            languages: this.props.languages,
            classes: this.props.classes
        });
    }

    componentWillReceiveProps(nexProps) {
        if (this.props !== nexProps) {
            this.setState({
                objects: nexProps.objects,
                languages: nexProps.languages,
                classes: nexProps.classes
            });
        }
    }

    authorChange = (event) => {
        this.setState({author: event.target.value});
    };

    bookNameChange = (event) => {
        this.setState({bookName: event.target.value});
    };


    bookYearChange = (event) => {
        this.setState({bookYear: event.target.value});
    };

    bookDescriptionChange = (event) => {
        this.setState({bookDescription: event.target.value});
    };

    bookKeyWordsChange = (event) => {
        this.setState({bookKeyWords: event.target.value});
    };

    bookPagesChange = (event) => {
        this.setState({bookPages: event.target.value});
    };

    activeObjectChange = (event, index, value) => {
        this.setState({activeObject: value});
    };

    activeLanguageChange = (event, index, value) => {
        this.setState({activeLanguage: value})
    };

    activeClassChange = (event, index, value) => {
        this.setState({activeClass: value})
    };

    fileChange = (event) => {
        this.setState({file: event.target.files[0]})
    };

    ntbCheckboxChange = () => {
        this.setState((oldState) => {
            return {
                ntbChecked: !oldState.ntbChecked,
            };
        });
    };

    kstuCheckboxChange = () => {
        this.setState((oldState) => {
            return {
                kstuChecked: !oldState.kstuChecked,
            };
        });
    };

    bookAdd = async () => {
        if(!this.state.bookName && !this.state.bookPages && !this.state.activeLanguage && !this.state.activeObject) {
            console.log('something wrong');
            return;
        }
        await this.bookUpload();

        let bookClass;

        if (this.state.activeClass === null)
            bookClass = '';
        else bookClass = this.state.activeClass;

        let book ={
            author: this.state.author,
            bookName: this.state.bookName,
            link: this.state.bookFileName,
            about: this.state.bookDescription,
            year: this.state.bookYear,
            object: this.state.activeObject,
            lyceum: bookClass,
            language: this.state.activeLanguage,
            kstuWork: '' + this.state.kstuChecked,
            ntbWork: '' + this.state.ntbChecked,
            keyWord: this.state.bookDescription,
            fileSize: this.state.bookSize,
            bookPages: this.state.bookPages,
        };

        this.props.addBook(book);
    };

    bookUpload = () => {
        const formData = new FormData();
        formData.append('file',this.state.file);
        let file = formData.get('file');
        let fileName = 'files/' + file.name;
        let fileSize = Math.round(file.size / 1000000);
        this.setState({
            bookFileName: fileName,
            bookSize: fileSize
        });

        this.props.uploadBook(formData);
    };

    render() {
        return(
            <div>
                <div className='Header'>Добавление книги</div>
                <div className='Main'>
                    <Input
                        floatText="Автор книги"
                        styles={inlineStyles.input}
                        onChange={this.authorChange}
                    />
                    <br/>
                    <Input
                        floatText="Название книги"
                        styles={inlineStyles.input}
                        onChange={this.bookNameChange}
                    />
                    <br/>
                    <Input
                        floatText="Год издания"
                        styles={inlineStyles.input}
                        onChange={this.bookYearChange}
                    />
                    <br/>
                    <Input
                        styles={inlineStyles.input}
                        hintText="Описание книги"
                        multiLine={true}
                        rows={2}
                        rowsMax={4}
                        onChange={this.bookDescriptionChange}
                    />
                    <br/>
                    <Input
                        styles={inlineStyles.input}
                        hintText="Ключевые слова"
                        multiLine={true}
                        rows={2}
                        rowsMax={4}
                        onChange={this.bookKeyWordsChange}
                    />
                    <br/>
                    <SelectField
                        floatingLabelText='Выберите рубрику'
                        value={this.state.activeObject}
                        onChange={this.activeObjectChange}
                        style={inlineStyles.dropdown}
                    >
                        {this.state.objects.map((item) => {
                            return <MenuItem key={item.name_object} value={item.name_object} primaryText={item.name_object}/>

                        })}
                    </SelectField>
                    <br/>
                    <SelectField
                        floatingLabelText='Выберите язык'
                        value={this.state.activeLanguage}
                        onChange={this.activeLanguageChange}
                        style={inlineStyles.dropdown}
                    >
                        {this.state.languages.map((item) => {
                            return <MenuItem key={item.id_lang} value={item.lang_name} primaryText={item.lang_name}/>

                        })}
                    </SelectField>
                    <br/>
                    <SelectField
                        floatingLabelText='Выберите класс лицея'
                        value={this.state.activeClass}
                        onChange={this.activeClassChange}
                        style={inlineStyles.dropdown}
                    >
                        {this.state.classes.map((item) => {
                            return <MenuItem key={item.id_lyceum} value={item.lyceum_name} primaryText={item.lyceum_name}/>

                        })}
                    </SelectField>
                    <br/>
                    <Input
                        styles={inlineStyles.input}
                        floatText="Страницы"
                        onChange={this.bookPagesChange}
                    />
                    <br/>
                    <div style={inlineStyles.block}>
                        <div style={inlineStyles.block2}>
                            <Checkbox
                                label="Труды КГТУ"
                                styles={inlineStyles.checkbox}
                                checked={this.state.kstuChecked}
                                onCheck={this.kstuCheckboxChange}
                            />
                            <br/>
                            <Checkbox
                                label="Учебники и метод. пособия"
                                checked={this.state.ntbChecked}
                                onCheck={this.ntbCheckboxChange}
                                style={inlineStyles.checkbox}
                            />
                            <br/>
                        </div>
                    </div>
                    <input  id='book' type='file' className='Input' onChange={this.fileChange}/>
                    <br/>
                    <Button
                        label='Добавить книгу'
                        styles={inlineStyles.button}
                        labelStyle={{color: 'white'}}
                        backgroundColor={{backgroundColor: '#2c98f0'}}
                        onClick={this.bookAdd}
                    />

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        classes: state.lyceums,
        objects: state.objects,
        languages: state.languages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        classesShow: () => dispatch(classActions.getLyceum()),
        objectsShow: () => dispatch(objectActions.getObjects()),
        languagesShow: () => dispatch(languageActions.getLanguages()),
        uploadBook: (file) => dispatch(bookActions.uploadBookRequest(file)),
        addBook: (book) => dispatch(bookActions.addBook(book))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookUpload);