import React, { PureComponent } from 'react';
import 'react-virtualized/styles.css';
import * as actions from "../../../actions/Statistics";
import {connect} from "react-redux";
import '../Books/BookUpload.css';
import Chart from 'chart.js';
import {Bar, Line, Pie} from 'react-chartjs-2';


class Usage extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dates: [],
            views: [],
            chartData: {}
        }
    }

    componentDidMount() {
        this.props.getVisits();
    }

    componentWillReceiveProps(nexProps) {
        if (this.props !== nexProps) {
            let arrayViews =[];
            let arrayDate =[];
            nexProps.statistics.map(item => {
                let formatDate = item.date.toString();
                arrayViews.push(item.views);
                arrayDate.push(formatDate.substr(0, formatDate.indexOf('T')));
            });
            this.setState({
                chartData:{
                    labels: arrayDate,
                    datasets:[
                        {
                            label:'Population',
                            data: arrayViews,
                            backgroundColor:[
                                'rgba(255, 99, 132, 0.6)',
                                'rgba(54, 162, 235, 0.6)',
                                'rgba(255, 206, 86, 0.6)',
                                'rgba(75, 192, 192, 0.6)',
                                'rgba(153, 102, 255, 0.6)',
                                'rgba(255, 159, 64, 0.6)',
                                'rgba(255, 99, 132, 0.6)'
                            ]
                        }
                    ]
                }
            });
        }
    }





    render() {
        return(
            <div>
                <div className="Header">Книги</div>
                <div className="Main">
                    <Line
                        data={this.state.chartData}
                        options={{
                            title:{
                                display: 'Статистика посещений',
                                text: 'Статистика посещений',
                                fontSize:25
                            },
                            legend:{
                                position:this.props.legendPosition
                            }
                        }}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        statistics: state.statistics
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getVisits: () => dispatch(actions.getVisits())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Usage);