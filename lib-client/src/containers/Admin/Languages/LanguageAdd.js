import React, { Component } from 'react';
import Input from '../../../components/UI/Controls/Input';
import Button from '../../../components/UI/Controls/Button';
import '../Books/BookUpload.css';
import {connect} from "react-redux";
import * as actions from "../../../actions/Languages";

const inlineStyles ={
    button: {
        verticalAlign: 'top',
        marginTop: '35px',
        marginBottom: '20px',
        width: '260px'
    },
};

class LanguageAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            language: ''
        };
    }

    languageChange = (event) => {
        this.setState({language: event.target.value});
    };

    addLanguage = () => {
        let data = {
            name: this.state.language
        };
        this.props.languageAdd(data);
    };

    render() {
        return(
            <div>
                <div className='Header'>Добавление языка</div>
                <div className='Main'>
                    <Input
                        floatText="Укажите язык для книг"
                        onChange={this.languageChange}
                    />
                    <br/>
                    <Button
                        label='Добавить'
                        onClick={this.addLanguage}
                        styles={inlineStyles.button}
                        labelStyle={{color: 'white'}}
                        backgroundColor={{backgroundColor: '#2c98f0'}}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        languages: state.languages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        languageAdd: (data) => dispatch(actions.addLanguage(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LanguageAdd);