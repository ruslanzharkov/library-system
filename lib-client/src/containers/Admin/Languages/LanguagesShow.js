import React, { PureComponent } from 'react';
import { Column, Table } from 'react-virtualized';
import 'react-virtualized/styles.css';
import * as actions from "../../../actions/Languages";
import _ from 'lodash';
import {connect} from "react-redux";
import '../Books/BookUpload.css';
import Loader from '../../../components/Loader/Loader';

class LanguagesShow extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            languages: []
        }
    }

    componentDidMount() {
        this.props.languagesShow();
    }

    componentWillReceiveProps(nextProps) {
        if(this.props !== nextProps) {
            this.setState({
                languages: nextProps.languages
            })
        }
    }

    render() {
        let languages = <Loader/>;
        if (!_.isEmpty(this.state.languages)) {
            languages = (
                <Table
                    width={900}
                    height={600}
                    headerHeight={20}
                    rowHeight={30}
                    rowCount={this.state.languages.length}
                    rowGetter={({ index }) => this.state.languages[index]}
                    overscanRowCount={5}
                >
                    <Column
                        label='№'
                        dataKey='id_lang'
                        width={100}
                    />
                    <Column
                        width={700}
                        label='Наименование языка'
                        dataKey='lang_name'
                    />

                </Table>
            )
        }

        return(
            <div>
                <div className="Header">Языки</div>
                <div className="Main">
                    {languages}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        languages: state.languages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        languagesShow: () => dispatch(actions.getLanguages())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LanguagesShow);