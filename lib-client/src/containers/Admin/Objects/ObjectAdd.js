import React, { Component } from 'react';
import Input from '../../../components/UI/Controls/Input';
import Button from '../../../components/UI/Controls/Button';
import '../Books/BookUpload.css';


const inlineStyles ={
    button: {
        verticalAlign: 'top',
        marginTop: '35px',
        marginBottom: '20px',
        width: '260px'
    },
};

class BookUpload extends Component {
    render() {
        return(
            <div>
                <div className='Header'>Добавление рубрики</div>
                <div className='Main'>
                    <Input
                        floatText="Наименование рубрики"
                    />
                    <br/>
                    <Button
                        label='Добавить'
                        onClick={this.searchBooksWithParameters}
                        styles={inlineStyles.button}
                        labelStyle={{color: 'white'}}
                        backgroundColor={{backgroundColor: '#2c98f0'}}
                    />
                </div>
            </div>
        )
    }
}

export default BookUpload;