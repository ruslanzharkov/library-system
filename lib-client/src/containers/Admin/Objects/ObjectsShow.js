import React, { PureComponent } from 'react';
import { Column, Table } from 'react-virtualized';
import 'react-virtualized/styles.css';
import * as actions from "../../../actions/Objects";
import _ from 'lodash';
import {connect} from "react-redux";
import '../Books/BookUpload.css';
import Loader from '../../../components/Loader/Loader';

class ObjectsShow extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            objects: []
        }
    }

    componentDidMount() {
        this.props.objectsShow();
    }

    componentWillReceiveProps(nextProps) {
        if(this.props !== nextProps) {
            this.setState({
                objects: nextProps.objects
            })
        }
    }

    render() {
        let books = <Loader/>;
        if (!_.isEmpty(this.state.objects)) {
            books = (
                <Table
                    width={900}
                    height={600}
                    headerHeight={20}
                    rowHeight={30}
                    rowCount={this.state.objects.length}
                    rowGetter={({ index }) => this.state.objects[index]}
                    overscanRowCount={5}
                >
                    <Column
                        label='№'
                        dataKey='id_object'
                        width={100}
                    />
                    <Column
                        width={700}
                        label='Наименование предмета'
                        dataKey='name_object'
                    />

                </Table>
            )
        }

        return(
            <div>
                <div className="Header">Предметы</div>
                <div className="Main">
                    {books}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        objects: state.objects
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        objectsShow: () => dispatch(actions.getObjects())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ObjectsShow);