import React, { PureComponent } from 'react';
import { Column, Table } from 'react-virtualized';
import 'react-virtualized/styles.css';
import * as actions from "../../../actions/Lyceum";
import _ from 'lodash';
import {connect} from "react-redux";
import '../Books/BookUpload.css';
import Loader from '../../../components/Loader/Loader';

class ShowLyceum extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            classes: []
        }
    }

    componentDidMount() {
        this.props.classesShow();
    }

    componentWillReceiveProps(nextProps) {
        if(this.props !== nextProps) {
            this.setState({
                classes: nextProps.classes
            })
        }
    }

    render() {
        let languages = <Loader/>;
        if (!_.isEmpty(this.state.classes)) {
            languages = (
                <Table
                    width={900}
                    height={600}
                    headerHeight={20}
                    rowHeight={30}
                    rowCount={this.state.classes.length}
                    rowGetter={({ index }) => this.state.classes[index]}
                    overscanRowCount={5}
                >
                    <Column
                        label='№'
                        dataKey='id_lyceum'
                        width={100}
                    />
                    <Column
                        width={700}
                        label='Класс'
                        dataKey='lyceum_name'
                    />

                </Table>
            )
        }

        return(
            <div>
                <div className="Header">Классы лицея</div>
                <div className="Main">
                    {languages}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        classes: state.lyceums
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        classesShow: () => dispatch(actions.getLyceum())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowLyceum);