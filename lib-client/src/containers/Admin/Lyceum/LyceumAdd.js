import React, { Component } from 'react';
import Input from '../../../components/UI/Controls/Input';
import Button from '../../../components/UI/Controls/Button';
import '../Books/BookUpload.css';
import {connect} from "react-redux";
import * as lyceumActions from "../../../actions/Lyceum";


const inlineStyles ={
    button: {
        verticalAlign: 'top',
        marginTop: '35px',
        marginBottom: '20px',
        width: '260px'
    },
};

class LyceumAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lyceum: ''
        };
    }

    lyceumChange = (event) => {
        this.setState({lyceum: event.target.value});
    };

    addLyceum = () => {
        let data = {
            name: this.state.lyceum
        };
        console.log(data);
        this.props.addLyceum(data);
    };

    render() {
        return(
            <div>
                <div className='Header'>Добавление класса</div>
                <div className='Main'>
                    <Input
                        floatText="Укажите класс"
                        onChange={this.lyceumChange}
                    />
                    <br/>
                    <Button
                        label='Добавить'
                        onClick={this.addLyceum}
                        styles={inlineStyles.button}
                        labelStyle={{color: 'white'}}
                        backgroundColor={{backgroundColor: '#2c98f0'}}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        lyceum: state.lyceums,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLyceum: (data) => dispatch(lyceumActions.addLyceum(data)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LyceumAdd);