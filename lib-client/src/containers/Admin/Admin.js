import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Navbar from '../Navbar/Navbar';
import Drawer from '../../components/UI/Drawer/Drawer';
import Menu from '../../components/UI/Menu/Menu';
import BookUpload from './Books/BookUpload';
import ObjectsShow from './Objects/ObjectsShow'
import ObjectAdd from './Objects/ObjectAdd';
import BooksShow from './Books/BooksShow';
import LyceumAdd from './Lyceum/LyceumAdd';
import ShowClasses from './Lyceum/ShowLyceum';
import ShowLanguages from './Languages/LanguagesShow';
import LanguageAdd from './Languages/LanguageAdd';
import Usage from './Statistics/Usage';

import './Admin.css';



class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerOpen: false
        }
    }

    componentDidMount() {
        this.props.history.replace('/admin/books');
    }

    showDrawer = () => {
        this.setState({drawerOpen: !this.state.drawerOpen});
    };

    showBooks = () => {
        this.props.history.replace('/admin/books');
    };

    showObjects = () => {
        this.props.history.replace('/admin/objects');
    };

    subjectAdding = () => {
        this.props.history.replace( '/admin/object-add');
    };

    bookAdding = () => {
        this.props.history.replace( '/admin/book-add');
    };

    showClasses = () => {
        this.props.history.replace('/admin/show-classes');
    };

    classAdding = () => {
        this.props.history.replace( '/admin/class-add');
    };

    showLanguages = () => {
        this.props.history.replace( '/admin/languages');
    };

    languageAdding = () => {
        this.props.history.replace('/admin/language-add');
    };

    usageShow = () => {
        this.props.history.replace('/admin/usage');
    };

    render() {
        return (
            <div className='AdminMain'>
                <Navbar
                  burgerClicked={this.showDrawer}
                />
            <div>
                <Menu
                    showBooks={this.showBooks}
                    bookAdd={this.bookAdding}
                    showObjects={this.showObjects}
                    subjectAdd={this.subjectAdding}
                    showClasses={this.showClasses}
                    classAdd={this.classAdding}
                    showLanguages={this.showLanguages}
                    languageAdd={this.languageAdding}
                    showUsage={this.usageShow}
                />
                <Route
                    path={this.props.match.path + '/books'}
                    component={BooksShow}
                />
                <Route
                    path={this.props.match.path + '/book-add'}
                    component={BookUpload} />
                <Route
                    path={this.props.match.path + '/objects'}
                    component={ObjectsShow}
                />
                <Route
                    path={this.props.match.path + '/object-add'}
                    component={ObjectAdd}
                />
                <Route
                    path={this.props.match.path + '/show-classes'}
                    component={ShowClasses}
                />
                <Route
                    path={this.props.match.path + '/class-add'}
                    component={LyceumAdd}
                />
                <Route
                    path={this.props.match.path + '/languages'}
                    component={ShowLanguages}
                />
                <Route
                    path={this.props.match.path + '/language-add'}
                    component={LanguageAdd}
                />
                <Route
                    path={this.props.match.path + '/usage'}
                    component={Usage}
                />
            </div>
            <Drawer
                open={this.state.drawerOpen}
                requestChange={(drawerOpen) => this.setState({drawerOpen})}
            />
          </div>
        );
    }
}

export default App;
