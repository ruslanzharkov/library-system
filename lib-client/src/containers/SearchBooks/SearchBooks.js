import React, { Component } from 'react';
import { connect } from 'react-redux';
import Fuse from 'fuse.js';
import _ from 'lodash';
import * as actions from '../../actions/Books';
import Input from '../../components/UI/Controls/Input';
import Button from '../../components/UI/Controls/Button';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Table from '../../components/UI/Table/Table';
import { searchParams } from '../../etc/SearchParams';
import './SearchBooks.css';
import { tableParams } from '../../etc/SearchTableProperties';
import Radio from '../../components/UI/Controls/Radio';
import Loader from '../../components/Loader/Loader';

const inlineStyles = {
    input: {
        width: '400px',
        display: 'inline-block',
        marginRight: '20px',
    },
    button: {
        verticalAlign: 'top',
        marginTop: '35px',
        marginLeft: '20px',
        marginBottom: '20px',
    },
    dropdown: {
        width: '220px',
        verticalAlign: 'top',
        marginTop: '24px',
    },
    table: {
        width: '90%',
        padding: '10px',
        margin: '30px auto'
    },
    div: {
        marginTop: '20px',
        display: 'flex', 
        flexDirection: 'row'
    },
    block: {
        display: 'flex',
        margin: 'auto',
        marginLeft: '300px',
        marginRight: '200px'
    },
    block2: {
        margin: 10,
    },

};


class SearchBooks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            searchText: '',
            error: false,
            errorText: '',
            propsBooks: '',
            books: [],
            dropdown: 'autor_books',
            key: 'autor_books',
            kstuBooksCheckBox: false,
            eBooksCheckbox: true,
            ntbBooksCheckbox: false,
            dataForSearch: [],
        }
    }

    async componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.props = nextProps.books;
            console.log(nextProps, 'nextProps');
            await this.setState({
                propsBooks: nextProps.books
            });

        }
    }

    searchTextChanger = (event) =>{
        const newValue = event.target.value;
        this.setState({
            searchText: newValue
        });

        if (this.state.error) {
            this.setState({
                errorText: '',
                error: false
            })
        }
    };

    getSearchParameter = async () => {
        if (this.state.eBooksCheckbox) {
            await this.props.getELibBooks();
            this.setState({
                dataForSearch: this.state.propsBooks
            });
            return;
        }

        if (this.state.kstuBooksCheckBox) {
            await this.props.getKstuBooks();
            this.setState({
                dataForSearch: this.state.propsBooks
            });
            return;
        }

        if (this.state.ntbBooksCheckbox) {
            await this.props.getNtbBooks();
            this.setState({
                dataForSearch: this.state.propsBooks
            });
        }
    };

     searchBooksWithParameters = async () => {
        if (!this.state.searchText) {
            this.setState({
                errorText: 'Вы ничего не ввели, пожалуйста попробуйте снова',
                error: true
            });
            return;
        }

        await this.setState({
            loading: true
        });

        await this.getSearchParameter();
        const threshold = this.state.key === 'autor_books' ? 0.2 : 0.6;

        let options = {
            shouldSort: true,
            findAllMatches: true,
            threshold: threshold,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: [
                this.state.key
            ]
        };
        let dataSearch = this.state.dataForSearch;
        console.log("state", dataSearch);
        let fuse = new Fuse(dataSearch, options);
        let result = fuse.search(this.state.searchText);
        await this.setState({
            books: result,
            loading: false
        })
    };

    changeSearchKey = (event, index, dropdown) => {
        this.setState({
            dropdown: dropdown,
            key: dropdown
        });
    };

    radioEBooksChange = () => {
        this.setState({
            eBooksCheckbox: true,
            kstuBooksCheckBox: false,
            ntbBooksCheckbox: false
        })
    };

    radioKSTUBooksChange = () => {
        this.setState({
            eBooksCheckbox: false,
            kstuBooksCheckBox: true,
            ntbBooksCheckbox: false
        })
    };

    radioNTBBooksChange = () => {
        this.setState({
            eBooksCheckbox: false,
            kstuBooksCheckBox: false,
            ntbBooksCheckbox: true
        })
    };

    render() {
        let books = <center><p>Вы ничего не искали</p></center>;
        if (!_.isEmpty(this.state.books)) {
            books = (
                <Table
                    styles={inlineStyles.table}
                    data={this.state.books}
                    labels={tableParams}
               /> 
            );
        }

        if(this.state.loading) {
            books = <Loader/>;
        }

        return(
            <div>
                <div className='SearchFormHeader'>Поиск электронных ресурсов</div>
                <div className='SearchForm'>
                    <div className='SearchField'>
                        <Input 
                            floatText='Введите для поиска'
                            styles={inlineStyles.input}
                            errorText={this.state.errorText}
                            onChange={this.searchTextChanger}
                        />
                        <SelectField
                            value={this.state.dropdown}
                            onChange={this.changeSearchKey}
                            style={inlineStyles.dropdown}
                        >
                            {searchParams.map((item) => {
                                return <MenuItem key={item.name} value={item.name} primaryText={item.value}/>
                                
                            })}
                        </SelectField>
                        <Button 
                            label='Поиск'
                            onClick={this.searchBooksWithParameters}
                            styles={inlineStyles.button}
                            labelStyle={{color: 'white'}}
                            backgroundColor={{backgroundColor: '#2c98f0'}}
                        />
                        <div style={inlineStyles.block}>
                            <div style={inlineStyles.block2}>
                                <Radio
                                        checked={this.state.eBooksCheckbox}
                                        onChange={this.radioEBooksChange}
                                        label="Электронные учебники"
                                />
                                <Radio
                                        checked={this.state.ntbBooksCheckbox}
                                        onChange={this.radioNTBBooksChange}
                                        label="Учебники и метод. пособия КГТУ"
                                />
                                <Radio
                                        checked={this.state.kstuBooksCheckBox}
                                        onChange={this.radioKSTUBooksChange}
                                        label="Труды КГТУ"
                                />
                            </div>
                        </div>
                    </div>
               </div>
                {books}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        books: state.books
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getELibBooks: () => dispatch(actions.getBooks()),
        getKstuBooks: () => dispatch(actions.getKSTUBooks()),
        getNtbBooks: () => dispatch(actions.getNTBBooks())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBooks);