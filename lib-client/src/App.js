import React, { Component } from 'react';
import Main from './containers/Main';
import LoginPage from "./containers/Login/LoginPage";
import Admin from "./containers/Admin/Admin";
import { Route, Switch } from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={LoginPage}/>
                    <Route path="/search" component={Main}/>
                    <Route path="/admin" component={Admin}/>
                </Switch>
            </div>
        );
    }
}

export default App;
