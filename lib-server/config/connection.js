const mysql = require('mysql');
const dbConfig = require('./db');

const connection = mysql.createConnection({
    host     : dbConfig.host,
    user     : dbConfig.user,
    password : dbConfig.password,
    database : dbConfig.database
});

connection.connect();

module.exports = connection;