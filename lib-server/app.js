const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const serverConfig = require('./config/server');
const dbConfig = require('./config/db');
const booksRoutes = require('./routes/books');
const userRoutes = require('./routes/user');
const objectRoutes = require('./routes/objects');
const languagesRoutes = require('./routes/languages');
const lyceumRouter = require('./routes/classes');
const visitsRouter = require('./routes/visits');

const app = express();

app.use(bodyParser.json());

app.use(cors({ origin: '*' })); // TODO: временно для dev
app.use('/files', express.static('public'));

app.use('/', booksRoutes);
app.use('/', userRoutes);
app.use('/', objectRoutes);
app.use('/', languagesRoutes);
app.use('/', lyceumRouter);
app.use('/', visitsRouter);

app.listen(serverConfig.port, function() {
    console.log(`Server is up and running at port ${serverConfig.port}`);
});