const express = require('express');
const connection = require('../config/connection');
const router = express.Router();

router.get('/objects', function(req, res) {
    connection.query("SELECT * FROM object", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});


module.exports = router;