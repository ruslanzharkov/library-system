const express = require('express');
const connection = require('../config/connection');
const router = express.Router();

router.get('/visits', (req, res) => {
   connection.query("SELECT * FROM visits" , (err, results, fields) => {
       if(err)
           res.send({error: err});
       res.send(results);
    })
});

module.exports = router;