const express = require('express');
const connection = require('../config/connection');
const router = express.Router();

router.get('/languages', function(req, res) {
    connection.query("SELECT * FROM language", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});

router.post('/language-add', (req, res) => {
    let language = {
        lang_name: req.body.name
    };
    connection.query('INSERT INTO language SET ?', language ,(err, results, fields) => {
        if(err)
            res.send({error: err});
        res.send({success: true});
    });
});

module.exports = router;