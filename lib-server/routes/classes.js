const express = require('express');
const connection = require('../config/connection');
const router = express.Router();

router.get('/lyceum', function(req, res) {
    connection.query("SELECT * FROM lyceum", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});

router.post('/lyceum-add', (req, res) => {
    let lyceum = {
        lyceum_name: req.body.name
    };
    connection.query('INSERT INTO lyceum SET ?', lyceum ,(err, results, fields) => {
        if(err)
            res.send({error: err});
        res.send({success: true});
    });
});


module.exports = router;