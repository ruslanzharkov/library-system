const express = require('express');
const bcrypt = require('bcrypt');
const jwt    = require('jsonwebtoken');
const _ = require('lodash');
const bson = require('bson');
const connection = require('../config/connection');
const router = express.Router();


let throwFailed = function (res, message) {
    return res.json({ success: false, message: message });
};
  
let generateToken = function (login) {
    return jwt.sign({ login: login }, 'library', { expiresIn: '86400' })
};

router.post('/authenticate', function(req, res) {
    const login = req.body.login;
    const password = req.body.password;

    if (_.isUndefined(login) || _.isUndefined(password)) {
        return throwFailed(res, 'Authentication failed. User not found.');
      }

    connection.query('SELECT * FROM users WHERE login = ? AND password = ?',
        { replacements: [login, password ], type: connection.QueryTypes.SELECT }
    ).then(user => {
        if (_.isEmpty(user)) {
            return throwFailed(res, 'Authentication failed. User not found.');
        }

        bcrypt.compare(password, user.password, function(errBcrypt, resBcrypt) {
            if (resBcrypt === false) {
              return throwFailed(res, 'Authentication failed. Wrong password.');
            }
            return res.json({
                token: generateToken(login),
                user: user[0]
            });
        });
    })
});

module.exports = router;