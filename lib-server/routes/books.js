const express = require('express');
const path = require('path');
const multer = require('multer');
const connection = require('../config/connection');
const router = express.Router();

router.get('/getbooks', function(req, res) {
    connection.query("SELECT * FROM books", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});

router.get('/getkstuworks', function(req, res) {
    connection.query("SELECT * FROM books WHERE work_KSTU_books = 'true'", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});

router.get('/getntbworks', function(req, res) {
    connection.query("SELECT * FROM books WHERE work_NTB_books = 'true'", (err, results, fields) => {
        if(err)
            throw err;
        res.send(results)
    });
});

router.get('/book/:id', function(req, res) {
    let id = req.params.id;
    connection.query('SELECT * FROM books WHERE `id_books` = ?', [id], (err, results, fields) => {
        if(err)
            throw err;
        let bookPath = results[0].link_books;
        res.send(bookPath);
    })
});

router.post('/add-book', (req, res) => {
    let book = {
        edit_books: true,
        autor_books: req.body.author,
        name_books: req.body.bookName,
        link_books: req.body.link,
        about_books:req.body.about,
        year_books: req.body.year,
        object_books: req.body.object,
        lyceum_books: req.body.lyceum,
        lang_books: req.body.language,
        work_KSTU_books: req.body.kstuWork,
        work_NTB_books: req.body.ntbWork,
        key_books: req.body.keyWord,
        size_books: req.body.fileSize,
        page_books: req.body.bookPages,
        dnl_books: 0,
        show_books: 0

    };
    connection.query('INSERT INTO books SET ?', book ,(err, results, fields) => {
        if(err)
            res.send({error: err});
        res.send({success: true});
    });
});

router.get('/files/:id', function(req, res) {
    let bookPath = req.params.id;
    res.sendFile(path.join(__dirname.replace('routes', '/files/' + bookPath)))
});

router.post('/files', (req, res) => {
    console.log('uploading---', req.body.key);
    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, ('./files'))
        },
        filename: function (req, file, cb) {
            let fileName = file.originalname.substr(0, file.originalname.indexOf('.'));
            let fileType = file.originalname.split('.').pop();
            cb(null, fileName + '.' + fileType)
        }
    });

    let upload = multer({ storage:storage }).single('file');
    upload(req,res, function(err) {
        if(err) {
            res.send(err);
        }
        console.log("done upload---");
        res.json({"status":"completed"});
    });
});

module.exports = router;